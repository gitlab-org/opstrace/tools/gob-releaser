# gob-releaser

A CLI to make changes to our infra repositories to release tagged versions of GitLab Observability Backend (GOB)

```text
➜  gob-releaser git:(main) ./release.sh --help
---
release.sh helps create code updates and related MRs for releasing tagged versions of GitLab Observability Backend

The following flags are required.
    --environment               One of "staging" or "prod", that targets which environment we're cutting a release for.
                                Use "all" to target both environments.

The following flags are optional.
    --goui-image                Path of the GitLab Observability UI docker image to bump to.

    --existing-clone-path       Path to an existing checkout of the code specific to the environment being updated.
                                When omitted, a new clone is created everytime this script runs.

    --release-version           Name of the tagged release version we want the code to updated to.
                                When omitted, we fetch the latest tag available on the GOB repository.

    --debug                     Toggle "set -x" to trace bash commands. Takes no arguments.

The following settings are extracted from environment variables.
    GITLAB_PRIVATE_TOKEN        Private access token to query details for https://gitlab.com/gitlab-org/opstrace/opstrace
    OPS_PRIVATE_TOKEN           Private access token to query details for https://ops.gitlab.net/opstrace-realm/environments
---
```

## How to(s)

### To update environment(s) to the latest GOB release
```shell
./release.sh --environment staging

# or
./release.sh --environment all
```

> The command above creates a fresh checkout of the repository in question inside your current working directory

### To update environment(s) to the latest GOB release, using an existing clone of the repository
```shell
./release.sh \
--environment staging \
--existing-clone-path <ABSOLUTE_PATH_TO_REPO_CLONE>
```

### To update environment(s) to a given release
```shell
./release.sh \
--environment staging \
--existing-clone-path <ABSOLUTE_PATH_TO_REPO_CLONE> \
--release-version <TAGGED_RELEASE_NAME>
```

### To update environment(s) with a new GOUI image
```shell
./release.sh \
--environment staging \
--goui-image <IMAGE_TAG>
```

> <IMAGE_TAG> is just the image identifier/SHA, not the entire image path. e.g. 5c73498b